vim.cmd([[
let g:gruvbox_improved_strings = 1
let g:gruvbox_improved_warnings = 1
let g:gruvbox_termcolors = 256
let g:gruvbox_contrast_dark = 'medium'
]])
vim.cmd("colorscheme gruvbox")
