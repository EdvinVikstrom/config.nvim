local Editor = {}

function Editor:setup(conf)
  Editor.config = conf
  Editor.win = vim.api.nvim_get_current_win()

  Editor:setup_config()
  Editor:setup_options()
  Editor:setup_autocmds()
  Editor:setup_keymaps()
  Editor:setup_commands()
  return self
end

function Editor:setup_config()
  local conf = Editor.config
  conf.options = (conf.options or {})
  conf.win_options = (conf.win_options or {})
  conf.autocmds = (conf.autocmds or {})
  conf.keymaps = (conf.keymaps or {})
end

function Editor:setup_options()
  for key, val in pairs(Editor.config.options) do
    vim.api.nvim_set_option(key, val)
  end

  for key, val in pairs(Editor.config.win_options) do
    vim.api.nvim_win_set_option(0, key, val)
  end
end

function Editor:setup_autocmds()
  for i, cmd in ipairs(Editor.config.autocmds) do
    Editor:add_autocmd(cmd)
  end
end

function Editor:setup_keymaps()
  for key, map in pairs(Editor.config.keymaps) do
    Editor:add_keymap(key, map)
  end
end

function Editor:setup_commands()
  vim.cmd("cnoreabbrev <expr> W ((getcmdtype() is# ':' && getcmdline() is# 'W')?('w'):('W'))")
  vim.cmd("cnoreabbrev <expr> Q ((getcmdtype() is# ':' && getcmdline() is# 'Q')?('q'):('Q'))")
  vim.cmd("command CloseBuf :lua require(\"editor\"):close_buffer()")
end

function Editor:add_autocmd(cmd)
  vim.api.nvim_create_autocmd(cmd.event, cmd.opts)
end

function Editor:add_keymap(key, map)
  vim.keymap.set(map.mode, key, map.rhs, (map.opts or {silent = true}))
end

function Editor:close_buffer()
  local buf_to_close = vim.api.nvim_get_current_buf()
  local bufs = vim.api.nvim_list_bufs()
  local listed = {}

  for i, buf in ipairs(bufs) do
    if (vim.api.nvim_buf_get_option(buf, "buflisted")) then
      table.insert(listed, buf)
    end
  end

  if (buf_to_close == listed[#listed]) then
    vim.cmd("bprev")
  else
    vim.cmd("bnext")
  end

  local buf = vim.api.nvim_get_current_buf()
  if (buf == buf_to_close) then
    buf = vim.api.nvim_create_buf(true, false)
    vim.api.nvim_set_current_buf(buf)
  end
  vim.api.nvim_buf_delete(buf_to_close, {})
end

return Editor
