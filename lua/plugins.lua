return require("packer").startup(function()
  use "wbthomason/packer.nvim"

  use {
    "morhetz/gruvbox", as = "gruvbox",
    config = function() require("conf.gruvbox") end
  }

  use {
    "hrsh7th/nvim-cmp",
    requires = {
      "neovim/nvim-lspconfig",
      "hrsh7th/cmp-nvim-lsp",
      "hrsh7th/cmp-buffer",
      "hrsh7th/cmp-path",
      "hrsh7th/cmp-cmdline",
      "L3MON4D3/LuaSnip"
    },
    config = function() require("conf.cmp") end
  }

  use {
    "/home/edvin/Projekt/statline", as = "statline",
    after = "gruvbox",
    config = function() require("conf.statline") end
  }

  use {
    "/home/edvin/Projekt/filetree", as = "filetree",
    after = {"gruvbox", "statline"},
    config = function() require("conf.filetree") end
  }
end)
