local Editor = require("editor")
Editor:setup({
  options = {
    shiftwidth = 2
  },
  win_options = {
    number = true,
    relativenumber = true,
  },
  keymaps = {
    ["<A-h>"] = {mode = "n", rhs = ":bprev<CR>"},
    ["<A-l>"] = {mode = "n", rhs = ":bnext<CR>"},
    ["<A-o>"] = {mode = "n", rhs = ":CloseBuf<CR>"},
    ["<C-h>"] = {mode = "n", rhs = "<C-w>h"},
    ["<C-j>"] = {mode = "n", rhs = "<C-w>j"},
    ["<C-k>"] = {mode = "n", rhs = "<C-w>k"},
    ["<C-l>"] = {mode = "n", rhs = "<C-w>l"},
    ["<A-j>"] = {mode = "n", rhs = ":+5<CR>"},
    ["<A-k>"] = {mode = "n", rhs = ":-5<CR>"},
    ["<A-S-j>"] = {mode = "n", rhs = ":+10<CR>"},
    ["<A-S-k>"] = {mode = "n", rhs = ":-10<CR>"},

    ["\"<BS>"] = {mode = "i", rhs = "\""},
    ["\""] = {mode = "i", rhs = "\"\"<left>"},
    ["'<BS>"] = {mode = "i", rhs = "'"},
    ["'"] = {mode = "i", rhs = "''<left>"},
    ["(<BS>"] = {mode = "i", rhs = "("},
    ["("] = {mode = "i", rhs = "()<left>"},
    ["[<BS>"] = {mode = "i", rhs = "["},
    ["["] = {mode = "i", rhs = "[]<left>"},
    ["{<BS>"] = {mode = "i", rhs = "{"},
    ["{"] = {mode = "i", rhs = "{}<left>"},
    ["{<CR>"] = {mode = "i", rhs = "{<CR>}<Esc>O"},
    ["{;<CR>"] = {mode = "i", rhs = "{<CR>};<Esc>O"}
  }
})

local plugins = require("plugins")

Editor:add_autocmd({
  event = "QuitPre",
  opts = {
    callback = function()
      local win = vim.api.nvim_get_current_win()
      if (win == Editor.win or (_G.filetree and win == _G.filetree.view.win)) then
	vim.cmd("qall")
      end
    end
  }
})
